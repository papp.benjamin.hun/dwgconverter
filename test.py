from shapely.geometry import Point,Polygon
import psycopg2


a= Point(4.0,5.0)
#b=Polygon([[p.x, p.y] for p in pointList])

a.to_wkt()
a.to_wkb()
conn= psycopg2.connect("dbname=dwgConverter user=postgres password=admin")
conn.autocommit = True
print(conn)
cur = conn.cursor()
cur.execute("SELECT name, ST_AsText(geom) FROM geometries;")
print(a.to_wkt())
rows= cur.fetchall()
for r in rows:
    print(f"id{r[0]} geometry{r[1]}")
pg_insert="""INSERT INTO geometries VALUES(id,%s,%s)"""
inserted_values=('DEFAULT','Point',a.to_wkt())
cur.execute(pg_insert,inserted_values)
cur.execute("INSERT INTO geometries VALUES('Point','POINT(5 5)');")
cur.close()
conn.close()