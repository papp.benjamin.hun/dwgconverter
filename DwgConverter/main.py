from re import L
import sys
import os
from shapely.geometry import Point, Polygon, LineString,MultiLineString,MultiPoint, multipoint
import psycopg2
from psycopg2 import sql
from shapely import affinity
import matplotlib.pyplot as plt
import sys
import trimesh
import random

a=Point(1.0,1.0)

class Literal(str):
	def __conform__(self, quote):
		return self
	@classmethod
	def mro(cls):
		return (object, )
	def getquoted(self):
		return str(self)

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
my_file = os.path.join(THIS_FOLDER, 'fullMap.dxf')
f= open(my_file,'r')
lines = f.readlines()
entitiesIndex=0
reachedEntities=False
wallsIndex=[]
filteredEquipmentsIndex=[]
endSequencesIndex=[]
entitiesIndex=[i for i in range(len(lines)) if lines[i].__contains__('ENTITIES')]
wallsIndex = [i for i in range(len(lines)) if lines[i].__contains__('WALL') and i>entitiesIndex[0]]
endSequencesIndex=[i for i in range(len(lines)) if lines[i].__contains__('SEQEND') and i>entitiesIndex[0]]
endSequencesIndexForSpecEquipment=[i for i in range(len(lines)) if lines[i].__contains__('SEQEND') and i<entitiesIndex[0]]
windowsIndex= [i for i in range(len(lines)) if lines[i].__contains__('WINDOW') and i>entitiesIndex[0]]
equipmentsIndex=[i for i in range(len(lines)) if lines[i].__contains__('EQUIPMENT') and i>entitiesIndex[0]]
railsIndex=[i for i in range(len(lines)) if lines[i].__contains__('RAIL') and i>entitiesIndex[0]]
linesIndex=[i for i in range(len(lines)) if lines[i].__contains__('LINE') and i>entitiesIndex[0]]
vertexIndex=[i for i in range(len(lines)) if lines[i].__contains__('VERTEX') and i>entitiesIndex[0]]
specialEquipmentIndex=[i for i in range(len(lines)) if lines[i].__contains__('$') and i>entitiesIndex[0]]

def specialEquipmentVertexExtract(index):
	#minden egyes special equipmentnek kiszedjük a vertexeit egy listába
	
	specialEquipmentIndexList=[]
	vertexInBlockIndexList=[]
	sortedIndexes=[]
	equipmentsList=[]
	for item in index:
		if(lines[int(item)-4].strip()=='INSERT'):
			equipmentsList.append(int(item)+2)
			sortedIndexes.append(int(item))
	global filteredEquipmentsIndex
	filteredEquipmentsIndex=sortedIndexes
	for item in equipmentsList:
		specialEquipmentIndexList.append([i for i in range(len(lines)) if lines[i].__contains__(lines[item]) and i<entitiesIndex[0]][0])

	for item in specialEquipmentIndexList:
		
		blockIndex=item
		endBlockIndex=item
		while(lines[blockIndex].strip()!="BLOCK"):
			blockIndex-=1

		while(lines[endBlockIndex].strip()!="ENDBLK"):
			endBlockIndex+=1

		vertexInBlockIndexUnitList=[i for i in range(len(lines)) if lines[i].strip()==('VERTEX') and i>blockIndex and i<endBlockIndex ]
		vertexInBlockIndexList.append(vertexInBlockIndexUnitList)
	return vertexInBlockIndexList

def specialEquipmentLinesExtract(index):
	equipmentsList=[]
	sortedIndexes=[]
	equipmentsSourceIndexList=[]
	linesInBlockIndexList=[]

	for item in index:
		if(lines[int(item)-4].strip()=='INSERT'):
			equipmentsList.append(int(item)+2)
			sortedIndexes.append(int(item))
	global filteredEquipmentsIndex
	filteredEquipmentsIndex=sortedIndexes
	
	for item in equipmentsList:
		equipmentsSourceIndexList.append([i for i in range(len(lines)) if lines[i].__contains__(lines[item]) and i<entitiesIndex[0]][0])

	for item in equipmentsSourceIndexList:
		
		blockIndex=item
		endBlockIndex=item
		while(lines[blockIndex].strip()!="BLOCK"):
			blockIndex-=1

		while(lines[endBlockIndex].strip()!="ENDBLK"):
			endBlockIndex+=1

		linesInBlockIndexUnitList=[i for i in range(len(lines)) if lines[i].strip()==('LINE') and i>blockIndex and i<endBlockIndex ]
		linesInBlockIndexList.append(linesInBlockIndexUnitList)
	return linesInBlockIndexList


def splitIndex(endIndexList,itemIndexList):
	pointList=[]
	prevSequenceIndex=0
	for item in endIndexList:
		AB=[x for x in itemIndexList if x > prevSequenceIndex and x<item]
		prevSequenceIndex=item
		if( AB.__len__() !=0):
			pointList.append(AB)
	return pointList

def lineExtractFromObjectType(index):
	insertedObjectLineStringList=[]
	for item in index:
		for item2 in item:
			lineIndex=item2

			while(lines[lineIndex+1].strip()!="10"):
				lineIndex+=1

			x1Coord=lines[lineIndex+2].strip()
			y1Coord=lines[lineIndex+4].strip()
			x2Coord=lines[lineIndex+8].strip()
			y2Coord=lines[lineIndex+10].strip()
			AB = LineString([Point(float(x1Coord),float(y1Coord)), Point(float(x2Coord),float(y2Coord))])
			insertedObjectLineStringList.append(AB)
	return insertedObjectLineStringList

def lineExtractFromObjectTypeWithOffset(index,specIndex):
	insertedObjectLineStringList=[]
	unitObjectLineStringList=[]
	for item in range(specIndex.__len__()):
		lineIndex=specIndex[item]
		scaleX=1
		scaleY=1
		scaleZ=1
		rotate=0
		offsetX1Coord=0
		offsetY1Coord=0
		

		while(lines[lineIndex].strip()!='0'):
			#print(lines[lineIndex].strip())
			if(lines[lineIndex].strip()=='10'):
				offsetX1Coord=float(lines[lineIndex+1].strip())
				offsetY1Coord=float(lines[lineIndex+3].strip())

			if(lines[lineIndex].strip()=='41'):
				scaleX=float(lines[lineIndex+1].strip())
			if(lines[lineIndex].strip()=='42'):
				scaleY=float(lines[lineIndex+1].strip())
			if(lines[lineIndex].strip()=='43'):
				scaleZ=float(lines[lineIndex+1].strip())
		
			if(lines[lineIndex].strip()=='50'):
				rotate=float(lines[lineIndex+1].strip())

			lineIndex+=1

		for item2 in index[item]:
			lineIndex=item2

			while(lines[lineIndex+1].strip()!="10"):
				lineIndex+=1

			x1Coord=lines[lineIndex+2].strip()
			y1Coord=lines[lineIndex+4].strip()
			x2Coord=lines[lineIndex+8].strip()
			y2Coord=lines[lineIndex+10].strip()
			AB = LineString([Point(float(x1Coord)+float(offsetX1Coord),float(y1Coord)+float(offsetY1Coord)),
				 Point(float(x2Coord)+float(offsetX1Coord),float(y2Coord)+float(offsetY1Coord))])
			AB=affinity.rotate(AB, rotate, Point(offsetX1Coord,offsetY1Coord), use_radians=False)
			AB=affinity.scale(AB, scaleX, scaleY, origin='center')
			unitObjectLineStringList.append(AB)
		insertedObjectLineStringList.append(unitObjectLineStringList)
		unitObjectLineStringList=[]
	return insertedObjectLineStringList

def PointList3DToPointList2D(pointList):
	pointList2D=[]
	finalList=[]
	for item in pointList:
		for item2 in item:
			for item3 in item2:
				pointList2D.append(item3)
		finalList.append(pointList2D)
		pointList2D=[]	
	return finalList

def vertexExtractFromObjectTypeWithOffset(index,specIndex):
	indexForIndividualVertexSequences=[]
	finalPointList=[]
	sequencePointList=[]
	elementPointList=[]
	pointList=[]
	superList=[]
	for item in index:
		indexForIndividualVertexSequences.append(splitIndex(endSequencesIndexForSpecEquipment,item))
	
	for item in range(specIndex.__len__()):
		lineIndex=specIndex[item]
		scaleX=1
		scaleY=1
		scaleZ=1
		rotate=0
		offsetX1Coord=0
		offsetY1Coord=0
		

		while(lines[lineIndex].strip()!='0'):
			#print(lines[lineIndex].strip())
			if(lines[lineIndex].strip()=='10'):
				offsetX1Coord=float(lines[lineIndex+1].strip())
				offsetY1Coord=float(lines[lineIndex+3].strip())

			if(lines[lineIndex].strip()=='41'):
				scaleX=float(lines[lineIndex+1].strip())
			if(lines[lineIndex].strip()=='42'):
				scaleY=float(lines[lineIndex+1].strip())
			if(lines[lineIndex].strip()=='43'):
				scaleZ=float(lines[lineIndex+1].strip())
		
			if(lines[lineIndex].strip()=='50'):
				rotate=float(lines[lineIndex+1].strip())

			lineIndex+=1
	
		
			
		for item2 in indexForIndividualVertexSequences[item]:
			for item3 in item2:
				vertexIndex=item3
		
				while(lines[vertexIndex+1].strip()!="10"):
					vertexIndex+=1

				x1Coord=float(lines[vertexIndex+2].strip())
				y1Coord=float(lines[vertexIndex+4].strip())
				AB = Point(x1Coord+offsetX1Coord,y1Coord+offsetY1Coord)
				if (float(x1Coord)!=0 and float(y1Coord)!=0):
					AB = Point(float(x1Coord),float(y1Coord))
					pointList.append(AB)
			if(len(pointList)>=2):
				line=LineString(pointList)
				line=affinity.rotate(line, rotate, Point(offsetX1Coord,offsetY1Coord), use_radians=False)
				line=affinity.scale(line, scaleX, scaleY, origin='center')
				sequencePointList.append(line)
				pointList=[]	
			elementPointList.append( sequencePointList)
			sequencePointList=[]
		finalPointList.append(elementPointList)
		elementPointList=[]
	return finalPointList
	

def convexHull(list1,list2):
	polygonList=[]
	list1PointList=[]
	list2PointList=[]
	list1AllPointList=[]
	list2AllPointList=[]
	for item in range(len(list1)):
		for item2 in range(len(list1[item])):
			for item3 in range(len(list1[item][item2].coords.xy[0])):
				list1PointList.append(Point(list1[item][item2].coords.xy[0][item3],list1[item][item2].coords.xy[1][item3]))
		list1AllPointList.append(list1PointList)
		list1PointList=[]

	for item in range(len(list2)):
		for item2 in range(len(list2[item])):
			for item3 in range(len(list2[item][item2].coords.xy[0])):
				list2PointList.append(Point(list2[item][item2].coords.xy[0][item3],list2[item][item2].coords.xy[1][item3]))
		list2AllPointList.append(list2PointList)
		list2PointList=[]

	for item in range(len(list2AllPointList)):
		a=MultiPoint([[p.x, p.y] for p in list1AllPointList[item]])
		b=MultiPoint([[p.x, p.y] for p in list2AllPointList[item]])
		c=[]
		for item in a:
			c.append(item)
		
		for item in b:
			c.append(item)

		c=MultiPoint([[p.x, p.y] for p in c])
		polygonList.append(c.convex_hull)
	return polygonList

def insertedElementExtract(index):
	list1=specialEquipmentLinesExtract(equipmentsIndex)
	lineElements=lineExtractFromObjectTypeWithOffset(list1,filteredEquipmentsIndex)  

	list=specialEquipmentVertexExtract(equipmentsIndex)
	list=vertexExtractFromObjectTypeWithOffset(list,filteredEquipmentsIndex)
	vertexElements=PointList3DToPointList2D(list)
	polyList=convexHull(vertexElements,vertexElements)
	polyList=polyList+convexHull(lineElements,lineElements)
	#vertexElements=(splitLineString(lineElements+vertexElements))

	return polyList

def lineExtractFromObjectName(data,index):
	if ( lines[index-4].strip()=="LINE"):
		while(lines[index].strip()!="10"):
			index+=1

		x1Coord=lines[index+1].strip()
		y1Coord=lines[index+3].strip()
		x2Coord=lines[index+7].strip()
		y2Coord=lines[index+9].strip()
		AB = LineString([Point(float(x1Coord),float(y1Coord)), Point(float(x2Coord),float(y2Coord))])
		return AB

def vertexExtract(indexList):
	pointList=[]
	for index in indexList:
		#index=data[item]
		if ( lines[index-4].strip()=="VERTEX"):
			while(lines[index].strip()!="10"):
				index+=1

			x1Coord=lines[index+1].strip()
			y1Coord=lines[index+3].strip()
			if (float(x1Coord)!=0 and float(y1Coord)!=0):
				AB = Point(float(x1Coord),float(y1Coord))
				pointList.append(AB)
	return pointList

def writeToDatabase(data,tableName):
	conn= psycopg2.connect("dbname=dwgConverter user=postgres password=admin")
	conn.autocommit = True
	cur = conn.cursor()
	polyList=[]
	

	for pItem in data:
		pg_insert="""INSERT INTO %s (name,geom) VALUES(%s,%s)"""
		inserted_values=(Literal(tableName),'Polygon',pItem.to_wkt())
		cur.execute(pg_insert,inserted_values)

	#for pItem in data:
	#	pg_insert="""INSERT INTO %s (name,geom) VALUES(%s,%s)"""
	#	inserted_values=(Literal(tableName),'Linestring',pItem.to_wkt())
	#	cur.execute(pg_insert,inserted_values)

	cur.close()
	conn.close()

def elementExtract(elementIndexList):
	lineElements=[]
	vertexElements=[]
	for index in elementIndexList:
		Line=lineExtractFromObjectName(lines,index)
		if(Line is not None):
			lineElements.append(Line)
	#lineElements=splitLineString(lineElements)
	

	#return elements
	pointList=splitIndex(endSequencesIndex,elementIndexList)
	for item in pointList:
		vertex=vertexExtract(item)
		if(len(vertex)!=0):
			vertexElements.append(LineString(vertex))
	#vertexElements=(splitLineString(lineElements+vertexElements))
	return vertexElements+lineElements

def IsTwoLineConnected(line1,line2):
	for item in line1.coords:
		for item2 in line2.coords:
			if(item==item2):
				return True

	return False

def splitLineString(lineStringList):
	firstXCoord=70000
	firstYCoord=70000
	lineStringList.sort(key=lambda t: pow(t.bounds[0]-firstXCoord,2)+pow(t.bounds[1]-firstYCoord,2))
	lineElements=[]
	a=[]
	a.append(lineStringList[0])
	lineElements.append(a)

	lineStringList.pop(0)
	isThereAConnection=False
	while(len(lineStringList)!=0):
		isThereAConnection=False
		currentLine=lineElements[len(lineElements)-1]
		for j in range(len(currentLine)):
			for k in range (len(lineStringList)):
				if(IsTwoLineConnected(currentLine[j],lineStringList[k])):
					lineElements[len(lineElements)-1].append(lineStringList[k])
					lineStringList.pop(k)
					isThereAConnection=True
					break
			else:
				continue
			break
		
		if(not isThereAConnection):
			a=[]
			a.append(lineStringList[0])
			lineElements.append(a)
			lineStringList.pop(0)
	x=[]
	for item in lineElements:	
		inlines = MultiLineString(item)
		outcoords = [list(i.coords) for i in inlines]
		outline = LineString([i for sublist in outcoords for i in sublist])
		x.append(outline)
	return x
def boundingBox(lineStringList):

	boundingBoxesList=[]
	for i in range(len(lineStringList)):
		minX=sys.float_info.max
		minY=sys.float_info.max
		maxX=sys.float_info.min*-1
		maxY=sys.float_info.min*-1
		for j in range(len(lineStringList[i].xy[0])):
			point=Point(lineStringList[i].xy[0][j],lineStringList[i].xy[1][j])
			if(point.x>maxX):
				maxX=point.x
			if(point.x<minX):
				minX=point.x

			if(point.y>maxY):
				maxY=point.y
			if(point.y<minY):
				minY=point.y
			
		pointList=[]
		pointList.append(Point(minX,maxY))
		pointList.append(Point(maxX,maxY))
		pointList.append(Point(maxX,minY))
		pointList.append(Point(minX,minY))
		pointList.append(Point(minX,maxY))
		poly=Polygon([[p.x, p.y] for p in pointList])
		if(poly.area>1 and poly.area<10000000):
			boundingBoxesList.append(poly)
		a=2
	return boundingBoxesList
def removeDuplicates(marker):
	sortedList=[]
	for item in marker:
		isPolyWithinAnother=False
		for item2 in marker:
			if(item != item2):
				poly1=item
				poly2=item2
				if(poly1.within(poly2)):
					isPolyWithinAnother=True
		if(not isPolyWithinAnother):
			sortedList.append(item)
	return sortedList
def LineStringToPolygon(list):
	polygonList=[]
	for i in range(len(list)):
		pointList=[]
		for j in range(len(list[i].xy[0])):
			pointList.append(Point(list[i].xy[0][j],list[i].xy[1][j]))
		if(len(pointList)>2):
			polygonList.append(Polygon([[p.x, p.y] for p in pointList]))
	return polygonList

def polygonsToGLB(list):
	scene = trimesh.Scene()
	for item in range(123):
		if list[item].geom_type=='Polygon' and list[item].bounds[0]>7000 and list[item].bounds[1]>7000 and list[item].bounds[2]<140000 and list[item].bounds[3]<100000 and list[item].is_valid:
			extrudedPolygon=trimesh.creation.extrude_polygon(list[item],1000)
			scene.add_geometry(extrudedPolygon)
			#scene.show()
	trimesh_scene = trimesh.Scene(geometry=scene)
	trimesh_scene.show()
	trimesh_scene.export(file_type="glb", file_obj="equipments8.glb")




importedEquipments=insertedElementExtract(equipmentsIndex)

equipments=elementExtract(equipmentsIndex)
equipments=boundingBox(equipments)
equipments=removeDuplicates(equipments)
#polygonsToGLB(equipments+importedEquipments)
walls=elementExtract(wallsIndex)
walls=LineStringToPolygon(walls)
polygonsToGLB(walls+equipments+importedEquipments)
#equipments=insertedElementExtract(equipmentsIndex)
#writeToDatabase(equipments,"equipments")

	


		





