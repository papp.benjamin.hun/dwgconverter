from shapely.geometry import Point, Polygon, LineString
from shapely import affinity
import matplotlib.pyplot as plt
import sys
offset=Point(17605485,22488119)
AB = Point(offset.x+87480,offset.y+10882)

A=affinity.rotate(AB, 180, offset, use_radians=False)
i=1

def IsTwoLineConnected(line1,line2):
	first1, last1 = line1.boundary
	first2,last2=line2.boundary
	if(first1==first2 or first1==last2 or last1==first2 or last1==last2):
		return True
	else:
		return False

def splitLineString(lineStringList):
	lineElements=[]
	a=[]
	a.append(lineStringList[0])
	lineElements.append(a)

	lineStringList.pop(0)
	isThereAConnection=False
	while(len(lineStringList)!=0):
		isThereAConnection=False
		currentLine=lineElements[len(lineElements)-1]
		for j in range(len(currentLine)):
			for k in range (len(lineStringList)):
				if(IsTwoLineConnected(currentLine[j],lineStringList[k])):
					lineElements[len(lineElements)-1].append(lineStringList[k])
					lineStringList.pop(k)
					isThereAConnection=True
					break
			else:
				continue
			break
		
		if(not isThereAConnection):
			a=[]
			a.append(lineStringList[0])
			lineElements.append(a)
			lineStringList.pop(0)
	return lineElements

def boundingBox(lineStringList):

	boundingBoxesList=[]
	for i in range(len(lineStringList)):
		minX=sys.float_info.max
		minY=sys.float_info.max
		maxX=sys.float_info.min*-1
		maxY=sys.float_info.min*-1
		for j in range(len(lineStringList[i].xy[0])):
			point=Point(lineStringList[i].xy[0][j],lineStringList[i].xy[1][j])
			if(point.x>maxX):
				maxX=point.x
			if(point.x<minX):
				minX=point.x

			if(point.y>maxY):
				maxY=point.y
			if(point.y<minY):
				minY=point.y
			
		pointList=[]

		pointList.append(Point(minX,maxY))
		pointList.append(Point(maxX,maxY))
		pointList.append(Point(maxX,minY))
		pointList.append(Point(minX,minY))
		pointList.append(Point(minX,maxY))
		q=pointList[0].boundary
		pointList[0].bounds[0]

		pointList.sort(key=lambda t: pow(t.bounds[0]-pointList[0].bounds[0],2)+pow(t.bounds[1]-pointList[0].bounds[1],2))
		poly=Polygon([[p.x, p.y] for p in pointList])
		plt.plot(*poly.exterior.xy)
		boundingBoxesList.append(poly)
		a=2
	return boundingBoxesList



	
testList=[]
testList.append(LineString([Point(0,0),Point(1,1)]))
testList.append(LineString([Point(0,1),Point(2,0)]))
testList.append(LineString([Point(3,0),Point(0,0)]))
testList.append(LineString([Point(4,4),Point(5,5)]))
testList.append(LineString([Point(5,5),Point(6,6)]))
#splitLineString(testList)
boundingBox(testList)
a=2

