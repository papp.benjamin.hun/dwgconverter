import os
import trimesh

from shapely.geometry import Point, Polygon, LineString
THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
scene = trimesh.Scene()
offset=-100000
pointList1=[Point(135542.2782424893,-98513.3425224798),Point(135019.6462406816,-98055.7120201783),Point(134642.2782424893,-96813.34252247977),Point(134642.2782424893,-95013.34252247977),Point(135142.2782424893,-94313.34252247977),Point(136592.2782424893,-98113.3425224798),Point(136492.2782424893, -98513.3425224798),Point(135542.2782424893, -98513.3425224798)]
#pointList2=[Point(-2+offset,0+offset),Point(-1+offset,1+offset),Point(-1+offset,-1+offset)]
poly1=Polygon([[p.x, p.y] for p in pointList1])
#poly2=Polygon([[p.x, p.y] for p in pointList2])
if(poly1.geom_type=='Polygon'):
	a=2

a1=trimesh.creation.extrude_polygon(poly1,2)
#a2=trimesh.creation.extrude_polygon(poly2,2)
scene.add_geometry(a1)
#scene.add_geometry(a2)
trimesh.exchange.gltf.export_glb(scene)
scene.show()
trimesh_scene = trimesh.Scene(geometry=scene)
trimesh_scene.show()
trimesh_scene.export(file_type="glb", file_obj="equipments11.glb")
  
